
public class Programmer extends Person {
	String tecknology;
	
	public Programmer() {
		
	}
	
	public Programmer(String name, String addrest, String tecknology) {
		super(name, addrest);
		this.tecknology = tecknology;
	}

	void hacking() {
		System.out.println("I can hacking a website");
	}
	
	void coding() {
		System.out.println("i can coding "+tecknology+".");
	}
	
	void greeting() {
		super.greeting();
		System.out.println("My job is a "+tecknology+"programmer.");
	}
}
