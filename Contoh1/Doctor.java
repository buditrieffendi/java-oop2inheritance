
public class Doctor extends Person {
	String specialist;
	
	public Doctor() {
		
	}
	
	public Doctor(String name, String addrest, String specialist) {
		super(name, addrest);
		this.specialist = specialist;
	}

	void surgery() {
		System.out.println("I can surgary operation patients");
	}

	void greeting() {
		super.greeting();
		System.out.println("My occupation is a"+specialist+" doctor");
	}
}
