
public class Teacher extends Person {
	String subject;
	
	public Teacher(){
		
	}
	
	public Teacher(String name, String addrest, String subject) {
		super(name, addrest);
		this.subject = subject;
	}

	void teaching() {
		System.out.println("I can teact "+subject+".");
	}
	
	void greeting() {
		super.greeting();
		System.out.println("My job is a "+subject+".");
	}
}
