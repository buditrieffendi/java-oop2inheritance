
public class AppMain {
	public static void main(String[] args) {
		
		Person person1 = new Person();
		person1.name = "Hendra";
		person1.addrest = "Garut";
		
		Teacher teacher1 = new Teacher();
		teacher1.name = "Budi";
		teacher1.addrest = "Bandung";
		teacher1.subject = "Matematika";
		
		Doctor doctor1 = new Doctor();
		doctor1.name = "Elis";
		doctor1.addrest = "Jakarta";
		doctor1.specialist = "Dentis";
		
		//membuat object menggunakan contractor default
		Programmer programmer1 = new Programmer();
		programmer1.name = "Rizky";
		programmer1.addrest = "Surabaya";
		programmer1.tecknology = "Java";
		
		//membuat object menggunakan contractor berparameter
		Programmer programmer2 = new Programmer("Jaja","Hilir","Java");
		
		person1.greeting();
		System.out.println();
		teacher1.greeting();
		System.out.println();
		doctor1.greeting();
		System.out.println();
		programmer1.greeting();
		System.out.println();
		programmer2.greeting();
		System.out.println();
		}
}
